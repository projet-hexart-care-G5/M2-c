//
// Created by matth on 09/11/2018.
//

#include <stdio.h>
#include "../includes/menu.h"
#include "../includes/generationCode.h"


void answer() {

    int continuer = 1;
    int r = 0;

    while(continuer) {

        printf("Configurer le clignotement des LEDs du coeur :\n");
        printf(" - Taper 1 : Mode chenille\n");
        printf(" - Taper 2 : 1 LED sur 2\n");
        printf(" - Taper 3 : 1 LED sur 3\n");
        printf(" - Taper 4 : 1 LED sur 4\n");
        printf(" - Taper 5 : 1 LED sur 5\n");
        printf(" - Taper 6 : Toutes les LEDs\n");
        printf(" - Taper 7 : Mode coupe\n");
        printf(" - Taper 0 : Pour quitter\n");

        scanf("%d", &r);

        switch (r) {
            case 0:
                continuer = 0;
                break;
            case 1:
                fill_file("param.h", r);
                break;
            case 2:
                fill_file("param.h", r);
                break;
            case 3:
                fill_file("param.h", r);
                break;
            case 4:
                fill_file("param.h", r);
                break;
            case 5:
                fill_file("param.h", r);
                break;
            case 6:
                fill_file("param.h", r);
                break;
            case 7:
                fill_file("param.h", r);
                break;
            default:
                break;
        }
    }

}