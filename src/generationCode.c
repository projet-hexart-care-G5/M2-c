//
// Created by matth on 09/11/2018.
//

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include "../includes/generationCode.h"


void fill_file(char *path_name, int value) {

    FILE *fp;
    char str[15];

    sprintf(str, "#define MODE %d" , value);

    fp = fopen(path_name, "w");
    fwrite(str, 1, sizeof(str), fp);

    fclose(fp);

}

